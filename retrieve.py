from google.cloud import firestore
from flask import make_response, jsonify

def retrieve_relay_request(request):
    """Responds to any HTTP request.
    Args:
        request (flask.Request): HTTP request object.
    Returns:
        The response text or any set of values that can be turned into a
        Response object using
        `make_response <http://flask.pocoo.org/docs/1.0/api/#flask.Flask.make_response>`.
    """
    request_json = request.get_json()

    requested_host = request_json['host']

    db = firestore.Client()

    doc_ref = db.collection(u'relay').document(requested_host)

    response_status_code = 200

    try:
        doc = doc_ref.get()
        data = doc.to_dict()
    except google.cloud.exceptions.NotFound:
        data = u'Invalid host!'
        response_status_code = 400

    json_response = jsonify(data)
    response = make_response((json_response, response_status_code))

    return response
