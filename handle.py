from google.cloud import firestore
from flask import make_response, jsonify

def handle_relay_request(request):
    """Responds to any HTTP request.
    Args:
        request (flask.Request): HTTP request object.
    Returns:
        The response text or any set of values that can be turned into a
        Response object using
        `make_response <http://flask.pocoo.org/docs/1.0/api/#flask.Flask.make_response>`.
    """
    request_json = request.get_json()

    request_full_path = request.full_path

    data = {
        u'payload': request_json,
        u'full_path': request_full_path,
        u'headers': dict(request.headers),
        u'remote_addr' :request.remote_addr,
        u'timestamp': firestore.SERVER_TIMESTAMP
    }

    db = firestore.Client()

    db.collection(u'relay').add(data)

    response_json = jsonify(dict(status='Created'))

    return make_response((response_json, 201))
